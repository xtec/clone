# Clone

Script per clonar i "update" tots els projectes del group `xtec`

```sh
$ git clone https://gitlab.com/xtec/clone
$ cd clone
$ ./clone.sh
```

