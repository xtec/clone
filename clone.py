import git
import gitlab
import os

group_id = "xtec"

if not os.path.exists(group_id):
    os.makedirs(group_id)

gl = gitlab.Gitlab()
group = gl.groups.get(group_id)


projects = group.projects.list(iterator=True, include_subgroups=True)
for project in projects:
    namespace = project.namespace["full_path"]
    path = project.path_with_namespace
    if not os.path.exists(path):
        print(f"{path}: cloning ...")
        git.Repo.clone_from(project.http_url_to_repo, path)
    else:
        repo = git.Repo(path)
        print(f"{path}: pulling ...")
        repo.remotes.origin.pull()
