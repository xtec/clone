#!/bin/bash

if ! command -v pip &> /dev/null; then
    sudo apt update
    sudo apt install -y python3-pip python3-venv
fi

if [ ! -f ".venv/bin/gitlab" ]; then
    python3 -m venv .venv
    .venv/bin/pip install --upgrade pip
    .venv/bin/pip install -r requirements.txt
fi

source .venv/bin/activate
python3 clone.py